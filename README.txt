This module uses the Field Group api to create a formatter that delimits 
multiple fields. This is distinct from Field Delimiter as this acts across 
multiple fields in a field group, not on single, multiple cardinality fields.
